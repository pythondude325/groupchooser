"use strict";

function filter_take(list) {

}

// A Fisher-Yates shuffle with cryptographic-grade random numbers
function shuffle(list) {
    const length = list.length;

    const random_numbers = window.crypto.getRandomValues(new Uint32Array(length));

    for (let i = length - 1; i >= 1; i--) {
        const j = random_numbers[i] % i;

        const temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }

    return list;
}


document.addEventListener("DOMContentLoaded", () => {
    const name_entry = document.getElementById("name-entry");
    const method_select = document.getElementById("grouping-method-select");
    const number_entry = document.getElementById("number-entry");
    const rounding_select = document.getElementById("rounding-select");
    const johns_checkbox = document.getElementById("johns-checkbox");
    const generate_button = document.getElementById("generate-button");
    const status_label = document.getElementById("status-label");
    const results_container = document.getElementById("results-container");

    function set_error(error_message) {
        if (error_message !== null) {
            status_label.innerText = error_message;
            status_label.style.visibility = "visible";
        } else {
            status_label.style.visibility = "hidden";
        }
    }

    function get_form_data() {
        if (name_entry.value.trim() == "") {
            return { error: "Names are required." };
        }

        const names = name_entry.value.trim().split("\n");

        if (method_select.value == "") {
            return { error: "A grouping method is required." };
        }

        const group_method = method_select.value;

        if (number_entry.value == "") {
            return { error: "A number is required." };
        }

        const grouping_number = parseInt(number_entry.value);

        if (grouping_number < 1) {
            return { error: "The number must be a positive integer." };
        }

        const rounding_method = rounding_select.value;
        const johns = johns_checkbox.checked;

        return { names, group_method, grouping_number, rounding_method, johns };

    }

    generate_button.addEventListener("click", () => {
        const form_data = get_form_data();

        if (form_data.error !== undefined) {
            set_error(form_data.error);
            return;
        } else {
            set_error(null);
        }

        let shuffled_names = shuffle(Array.from(form_data.names));
        let number_of_names = shuffled_names.length;


        let groups = [];

        if (form_data.johns) {
            let johns_group = [];

            for (let i = 0; i < shuffled_names.length; i++) {
                const name = shuffled_names[i];

                if (name.toLowerCase().match(/john/) || name.toLowerCase().match(/jonny/)) {
                    johns_group.push(name);
                    shuffled_names[i] = null;
                }
            }

            shuffled_names = shuffled_names.filter(n => n != null);

            groups.push(johns_group);

            number_of_names = shuffled_names.length;
        }

        let number_of_groups = null;

        if (form_data.group_method == "number-of-groups") {
            number_of_groups = form_data.grouping_number;
        } else if (form_data.group_method == "number-of-people") {
            if (form_data.rounding_method == "round-up") {
                number_of_groups = Math.floor(number_of_names / form_data.grouping_number);
            } else {
                number_of_groups = Math.ceil(number_of_names / form_data.grouping_number);
            }
        }

        let new_groups = [...Array(number_of_groups)].map(_ => []);

        for (let i = 0; i < number_of_names; i++) {
            new_groups[i % number_of_groups].push(shuffled_names[i]);
        }

        groups.push(...new_groups);

        results_container.innerHTML = "";

        groups.forEach((group, index) => {
            let header = document.createElement("div");
            header.innerText = `Group ${index + 1}`;
            results_container.appendChild(header);

            let list = document.createElement("ul");
            group.forEach((name) => {
                let list_item = document.createElement("li");
                list_item.innerText = name;
                list.appendChild(list_item);
            });
            results_container.appendChild(list);
        });

    })

});